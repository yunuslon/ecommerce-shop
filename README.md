<h1><a href="https://gitlab.com/yunuslon/ecommerce-shop">Web Ecommers Shayna Customer</a></h1>

## Page Home
<img src="https://gitlab.com/yunuslon/ecommerce-shop/-/raw/master/img_prt/Home.png" alt="drawing" width="600"/>
<p>Page Home adalah tempat informasi barang yang akan di beli oleh customer, yang dimana data yang berada </p>
<p>dalam halaman ini di peroleh dari web CMS ecommerce admin</p>

## Page Product
<img src="https://gitlab.com/yunuslon/ecommerce-shop/-/raw/master/img_prt/product.png" alt="drawing" width="600"/>
<p>Page Product berfungsi untuk menampilkan detail product seperti foto foto dari product yang di pilih</p>

## Page checkout
<img src="https://gitlab.com/yunuslon/ecommerce-shop/-/raw/master/img_prt/checkout.png" alt="drawing" width="600"/>
<p>Page Checkout berfungsi untuk menampung data data product yang akan di beli oleh Customer, </p>
<p>disini juga tempat Customer memasukan biodata mereka untuk keperluan pembayaran dan pengiriman barang</p>
<p>Data data yang dimasukan customer akan otomatis akan terkirim ke web ecommerce admin untuk diproses lebih lanjut</p>

## Succes page
<img src="https://gitlab.com/yunuslon/ecommerce-shop/-/raw/master/img_prt/success.png" alt="drawing" width="600"/>

<h1><a href="https://gitlab.com/yunuslon/ecommerce-backend">Web CMS Shayna Admin </a></h1>
    


<br/><br/><br/><br/><br/>

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

